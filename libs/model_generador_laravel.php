<?php   
/*++++++++++++++++++++++++++++++++++++++++++++++++
aporte por angelo romero ++++++++++++++++++++++++
visita 
https://bitbucket.org/angeloromero/botgen+++++++++++
para tener mans info sobre el proyecto++++++++++++++
gracias por tu visita y aporte  :D  +++++++++++++++
*/
function generadorlara($tabla,$primary_key,$campos){

  
    $output="";
    $output.="<?php"."\n";
    $output.=" class ".ucfirst(str_replace('_','',$tabla)). " extends Eloquent  { \n";
    $output.='protected $table ='."'".$tabla."'".";\n";
    $output.='protected $primaryKey ='."'".$primary_key."'".";\n";
    $output.='public $timestamps = false;'."\n";
    $output.='protected $fillable=[' ."\n";      
      foreach($campos as $campo){
         if($campo!=$primary_key){
         $output.='"'.$campo.'",' ."\n";
         }
      
      }
    $output.='];' ."\n";    
    $output.="\n";
    
    $output.=" } \n";
    
    
    
    return $output;
}
?>