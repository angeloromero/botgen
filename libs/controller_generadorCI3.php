<?php 
/*++++++++++++++++++++++++++++++++++++++++++++++++
aporte por angelo romero ++++++++++++++++++++++++
visita 
https://bitbucket.org/angeloromero/botgen+++++++++++
para tener mans info sobre el proyecto++++++++++++++
gracias por tu visita y aporte  :D  +++++++++++++++
*/
function generador_comt($tabla,$primary_key,$campos){

    $outcont="";
    	//$output.='<?php session_start();'
    $outcont.="<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');". "\n";
    $outcont.="session_start();". "\n";
    $outcont.=" class ".ucfirst($tabla). " extends CI_Controller { \n";
    $outcont.="function __construct(){". "\n";
    $outcont.="parent::__construct();". "\n";
    $outcont.='$this->load->model("'.$tabla.'_model");'. "\n";
    $outcont.="}". "\n";
    $outcont.="function index(){". "\n";
    $outcont.='$this->load->library("pagination");'. "\n";
    $outcont.='$config = array();'. "\n";
    $outcont.='$data = array();'. "\n";
    $outcont.='$config["base_url"]';
    $outcont.="= site_url('".$tabla;
    $outcont.="/index');". "\n";
    $outcont.='$config["total_rows"]';
    $outcont.='= $this->'.$tabla.'_model->count_all();'. "\n";
    $outcont.='$config["per_page"] = 5;'. "\n";
    $outcont.='$data = array();'. "\n";        
    $outcont.='$this->pagination->initialize($config);'. "\n";        
    $outcont.='$offset = $this->uri->segment(3);'. "\n";        
    $outcont.='$data["'.$tabla.'"] = $this->'.$tabla.'_model->get_all();'. "\n";        
    $outcont.='layout_interna("'.$tabla.'/index';
    $outcont.='",$data);'. "\n";        
    $outcont.="}". "\n";
    
    $outcont.="function add(){". "\n";
    $outcont.='layout_interna("'.$tabla."/add";
    $outcont.='");'. "\n";        
    $outcont.="}". "\n";
    
    
    $outcont.="function do_add(){". "\n";
    $outcont.='if ($this->input->post("do_action") === FALSE)'. "\n";
    $outcont.='redirect("'.$tabla.'");'. "\n";
    $outcont.='$data = array();'. "\n";    
    foreach($campos as $fields){
        
        if($fields!=$primary_key){
    $outcont.='$data["'.$fields.'"] = $this->input->post("'.$fields.'");'. "\n";
        }
        }    
    $outcont.='$this->'.$tabla.'_model->insert($data);'. "\n";
    $outcont.='redirect('.$tabla.');'. "\n";  
    $outcont.="}". "\n";

	
    $outcont.="function edit(){". "\n";
    $outcont.='if ($this->uri->segment(3) === FALSE)'. "\n";
    $outcont.='redirect("'.$tabla.'");'. "\n";
    $outcont.='$id = (int)$this->uri->segment(3);'. "\n";
    $outcont.='$'.$tabla.'= $this->'.$tabla.'_model->get_by_id($id);'. "\n";
    $outcont.='$data = array();'. "\n";    
    foreach($campos as $fields){
        
    $outcont.='$data["'.$tabla.'"]["'.$fields.'"] = $'.$tabla.'->'.$fields.';'. "\n";
    }    
    $outcont.='layout_interna("'.$tabla.'/edit';
    $outcont.='", $data);'. "\n"; 
    $outcont.="}". "\n";
    
    
    $outcont.="function do_edit(){". "\n";
//    $outcont.='if ($this->input->post("do_action") === FALSE)'. "\n";
   // $outcont.='redirect("'.$tabla.'");'. "\n";
    $outcont.='$data = array();'. "\n";    
    foreach($campos as $fields){
    $outcont.='$data["'.$fields.'"] = $this->input->post("'.$fields.'");'. "\n";
    }    
$outcont.='$this->'.$tabla.'_model->update($data,"$_post['.$primary_key.']");'. "\n";
    $outcont.='redirect("'.$tabla.'");'. "\n";  
    $outcont.="}". "\n";
    
    
    
    
    
    
    
    
    

    
    $outcont.="function delete(){". "\n";
    $outcont.='if ($this->uri->segment(3) === FALSE)'. "\n";
    $outcont.='redirect("'.$tabla.'");'. "\n";
    $outcont.='$id = (int)$this->uri->segment(3);'. "\n";
    $outcont.='$this->'.$tabla.'_model->delete($id);'. "\n";
    $outcont.='redirect("'.$tabla.'");'. "\n";  
    $outcont.="}". "\n";
    
    


    
    $outcont.="}". "\n";
    
    return $outcont;
}
?>