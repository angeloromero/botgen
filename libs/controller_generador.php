<?php 
/*++++++++++++++++++++++++++++++++++++++++++++++++
aporte por angelo romero ++++++++++++++++++++++++
visita 
https://bitbucket.org/angeloromero/botgen+++++++++++
para tener mans info sobre el proyecto++++++++++++++
gracias por tu visita y aporte  :D  +++++++++++++++
*/
function generador_comt($tabla,$primary_key,$campos){
    $tablasinespacio=str_replace('_','',$tabla);
    $tablasinespacio2= ucfirst(str_replace('_','',$tabla));   
   
    $model = ucfirst($_POST["tablas_db"])."_model"; 
    $outcont="";
    //$output.='<?php session_start();'
    $outcont.="<?php \n";
    $outcont.="namespace App\Controllers;

   //use CodeIgniter\Controller;". "\n";

   $outcont.= '//RUTAS INCLUIR MODELO EN BaseController PARA PRECARGA (opcional)'."\n\n";
   $outcont.= '// $this->'. $model.' = new '. $model.'();'."\n\n\n";
  


   $outcont.= '//RUTAS INCLUIR EN ROUTE'."\n";
   $outcont.= '//$routes->get("'.$tablasinespacio.'", "'.$tablasinespacio.'Controller::index");'."\n";
   $outcont.= '//$routes->get("'.$tablasinespacio.'/add", "'.$tablasinespacio2.'Controller::add");'."\n";
   $outcont.= '//$routes->get("'.$tablasinespacio.'/edit/(:num)", "'.$tablasinespacio2.'Controller::edit/$1");'."\n";
   $outcont.= '//$routes->get("'.$tablasinespacio.'/delete/(:num)", "'.$tablasinespacio2.'Controller::delete/$1");'."\n";
   $outcont.= '//$routes->post("'.$tablasinespacio.'/do_edit/",  "'.$tablasinespacio2.'Controller::do_edit");'."\n\n";
   $outcont.= '//$routes->post("'.$tablasinespacio.'/do_add/",  "'.$tablasinespacio2.'Controller::do_add");'."\n\n";

    $outcont.=" class ".$tablasinespacio2. "Controller  extends BaseController { \n";
    $outcont.="function __construct(){". "\n";
    $outcont.="helper(['is_logged_in']);". "\n";
	  $outcont.="is_logged_in();". "\n";
    $outcont.="}"."\n\n\n";
    

   
        
        

	  $outcont.= 'function index(){'."\n";   
    $outcont.= '$data = array();'."\n";
    $outcont.= '$builder = $this->db->table("'.$tabla.'");'."\n";
    $outcont.= '$query= $builder->get();'."\n";
	  $outcont.= '$data["'.$tabla.'"]=$builder->get()->getResult();'."\n";   
    $outcont.= 'return view("'.$tabla.'/index';
    $outcont.= '",$data);'."\n";        
    $outcont.= "}". "\n";
    
    $outcont.="function add(){". "\n";
    $outcont.='return view("'.$tabla."/add";
    $outcont.='");'. "\n";        
    $outcont.="}". "\n";
    
    
    $outcont.="function do_add(){". "\n";
        $outcont.='$input =$this->request->getPost();'."\n";
        $outcont.='$builder = $this->db->table("'.$tabla.'");'."\n";
        $outcont.='$data =['. "\n";    
        foreach($campos as $fields){
        $outcont.='"'.$fields.'" => $input["'.$fields.'"],'."\n";
        } 
        $outcont.='];'. "\n";         
      //$outcont.='$builder->where("ID", $input['.$primary_key.']);';
        $outcont.='$builder->insert($data);';                    
      //$outcont.='$this->'.$tabla.'_model->replace($data,"$_post['.$primary_key.']");'. "\n";
        $outcont.=' return redirect()->to("/'.$tablasinespacio.'");'. "\n";   
        $outcont.="}". "\n";


	
    $outcont.="function edit(){". "\n";
    $outcont.='$data = array();'. "\n";
    $outcont.='$id = $this->request->uri->getSegment(3);'. "\n";  
    $outcont.='$'.$tabla.'= $this->db->table("'.$tabla.'");'. "\n";
    $outcont.='$'.$tabla.'=$'.$tabla.'->where("ID", $id)->get()->getResult(); '. "\n";  
    foreach($campos as $fields){        
    $outcont.='$data["'.$tabla.'"]["'.$fields.'"] = $'.$tabla.'[0]->'.$fields.';'. "\n";
    }
    $outcont.='return view("'.$tabla.'/edit';
    $outcont.='", $data);'. "\n"; 
    $outcont.="}". "\n";
    
    
	
	
	
    $outcont.="function do_edit(){". "\n";
//    $outcont.='if ($this->input->post("do_action") === FALSE)'. "\n";
   // $outcont.='redirect("'.$tabla.'");'. "\n";
    $outcont.='$input =$this->request->getPost();'. "\n";
    $outcont.='$builder = $this->db->table("'.$tabla.'");'. "\n";
    $outcont.='$data =['. "\n";    
    foreach($campos as $fields){
    $outcont.='"'.$fields.'" => $input["'.$fields.'"],'. "\n";
    } 
    $outcont.='];'. "\n"; 
    
    $outcont.='$builder->where("ID", $input["'.$primary_key.'"]);';
    $outcont.='$builder->update($data);';                
                
  //  $outcont.='$this->'.$tabla.'_model->replace($data,"$_post['.$primary_key.']");'. "\n";
    $outcont.=' return redirect()->to("/'.$tablasinespacio.'");'. "\n";  
    $outcont.="}". "\n";
    
    
    
    
    
    
    
    
    

    
    $outcont.="function delete(){". "\n";



      $outcont.=' $id = (int) $id = $this->request->uri->getSegment(3);'. "\n"; 
      $outcont.=' $builder = $this->db->table("'.$tabla.'");'. "\n"; 
      $outcont.=' $builder->where("ID", $id);'. "\n"; 
      $outcont.=' $builder->delete();'. "\n"; 
      $outcont.=' return redirect()->to("/'.$tabla.'");'. "\n"; 

    
    $outcont.="}". "\n";
    $outcont.="}". "\n";
    
    return $outcont;
}
?>