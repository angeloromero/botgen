<?php
/*++++++++++++++++++++++++++++++++++++++++++++++++
aporte por angelo romero ++++++++++++++++++++++++
visita 
https://bitbucket.org/angeloromero/botgen+++++++++++
para tener mans info sobre el proyecto++++++++++++++
gracias por tu visita y aporte  :D  +++++++++++++++
*/
function index_gen($tabla,$keyprimaria,$campos){
  $asfore='';
  $asfore=substr($tabla, 0, -1); 
  $tablasinespacio=str_replace('_','',$tabla);
  $outindex='';

    
	 $outindex.='<?= $this->extend("layouts/headmasterlayout") ?>'. "\n"; 
	 $outindex.='<?= $this->section("content") ?>'. "\n"; 
	 $outindex.=" <!-- Breadcrumb -->"; 
	 $outindex.=' <nav class="hk-breadcrumb" aria-label="breadcrumb"> '. "\n";
	
	
	     $outindex.="<ol class='breadcrumb breadcrumb-light bg-transparent'> ". "\n";
			 $outindex.="<li class='breadcrumb-item'> ". "\n";; 
			 $outindex.="<a href='#'>Components "; 
			 $outindex.='</a>'. "\n";
			 $outindex.="</li> ". "\n";
			 $outindex.="<li class='breadcrumb-item active' aria-current='page'> ".$tabla.' - Index&nbsp;&nbsp;'."</li> ". "\n";
			 $outindex.="</ol> ". "\n";
			 $outindex.="</nav> ". "\n";
			 $outindex.="<!--table-hover w-100 display pb-30--> ". "\n";

			 $outindex.="<section class='hk-sec-wrapper'> ";
			 $outindex.='<a  href="'.$tablasinespacio.'/add"   class="btn btn-primary"> Agregar nuevo registro</a> '. "\n";
			 $outindex.="<div class='table-wrap' style='overflow-x: scroll;'>\n";
			 $outindex.="<table class='table'>\n";
	 
			 
			 $outindex.="<thead>\n";
			 
			foreach($campos as $campo){
			$outindex.='<th>'.$campo.'</th>'. "\n";

			}
		
			$outindex.='<th>Editar</th>'. "\n";
			$outindex.='<th>Borrar</th>'. "\n";    
			$outindex.='</tr>'. "\n";     
			$outindex.='</thead>'. "\n";     
			$outindex.='<tbody>'. "\n"; 
			 
			$outindex.='<?php foreach ($'.$tabla.' as $'.$asfore.') { ?>'. "\n";     
			$outindex.='<tr align="center">'. "\n"; 
			foreach($campos as $campo){
			$outindex.='<td><?php echo $'.$asfore.'->'.$campo.';?></td>'. "\n";
			}
		
		
			$outindex.='<td><?php echo anchor("'.$tablasinespacio;
      $outindex.="/edit/$" .$asfore.'->'.$keyprimaria.",'<i class=icon-pencil></i>',array(
      'class' => 'mr-25',
      'data-toggle'=>'tooltip',
      'data-original-title'=>'Editar registro'
    )); ?></td>"."\n";


    $outindex.='<td><?php echo anchor("'.$tablasinespacio;
      $outindex.="/delete/$" .$asfore.'->'.$keyprimaria.", '<i class=icon-trash txt-danger></i>',array(
      'class' => 'mr-25',
      'data-toggle'=>'tooltip',
      'data-original-title'=>'Borrar registro'
    )); ?></td>"."\n";


   

		
			$outindex.='</tr>'. "\n";     
			$outindex.='<?php } ?>'. "\n";     
			$outindex.='</tbody>'. "\n";         
			$outindex.='</table>'. "\n";         
			$outindex.='</div></div></section>	<?= $this->endSection() ?>'. "\n";

 
  return $outindex;    

}


function add_gen($tabla,$keyprimaria,$campos){
  $tablasinespacio=str_replace('_','',$tabla);
  $asfore='';
  $asfore=substr($tabla, 0, -1); 
  $edit_get='';
 
  $edit_get.=' <?= $this->extend("layouts/headmasterlayout") ?>'. "\n"; 
  $edit_get.='  <?= $this->section("content") ?>'. "\n"; 
  $edit_get.=' <!-- Breadcrumb -->'. "\n";    
  $edit_get.=' <nav class="hk-breadcrumb" aria-label="breadcrumb"> '. "\n"; 
  $edit_get.=' <ol class="breadcrumb breadcrumb-light bg-transparent">'. "\n"; 
  $edit_get.=' <li class="breadcrumb-item">'. "\n"; 
  $edit_get.=' <a href="#">Components </a>'. "\n"; 
  $edit_get.=' </li>'. "\n"; 
  $edit_get.=' <li class="breadcrumb-item active" aria-current="page"> '.$tabla.' - Add&nbsp;&nbsp;</li>'. "\n"; 
  $edit_get.=' </ol>'. "\n"; 
  $edit_get.=' </nav>'. "\n"; 


  $edit_get.='<section class="hk-sec-wrapper">'. "\n"; 
  $edit_get.=' <?=form_open("'.$tablasinespacio.'/do_add");?>'. "\n";
  $edit_get.='  <div class="position-center">'. "\n";
	  
  foreach($campos as $campo){
    if($campo==$keyprimaria){
     $edit_get.='<input type="hidden" name="'.$campo.'" id="'.$campo.'" value=""  class="form-control"  />'. "\n";      
    }else{    
  $edit_get.='<div class="form-group">'. "\n";
  $edit_get.='<label class="col-sm-3 control-label">'.$campo.'</label>'. "\n";
  $edit_get.='<div class="col-sm-8">'. "\n";
  $edit_get.='<input type="text" name="'.$campo.'" id="'.$campo.'" value=""  class="form-control"  />'. "\n";
  $edit_get.='</div>'. "\n";
  $edit_get.='</div>'. "\n";
    } 
  
  }  

  $edit_get.='<div class="form-group"><div class="col-lg-offset-2 col-lg-10">'. "\n"; 
  $edit_get.='<input type="submit" value="GUARDAR" class="btn btn-primary">	'. "\n";
  $edit_get.='<a class="btn btn-default" href="<?php echo site_url("'.$tablasinespacio.'");?>">VOLVER</a> </div></div>'. "\n";    
  $edit_get.='</form>'. "\n";	
  $edit_get.=' </div>'. "\n";	
  $edit_get.='</section>'. "\n";	
  $edit_get.='<?= $this->endSection() ?>'. "\n";	 
  return $edit_get;    

}




function edit_gen($tabla,$keyprimaria,$campos){
  $tablasinespacio=str_replace('_','',$tabla);
  $asfore='';
  $asfore=substr($tabla, 0, -1); 
  $edit_get='';

  $edit_get.=' <?= $this->extend("layouts/headmasterlayout") ?>'. "\n"; 
  $edit_get.='  <?= $this->section("content") ?>'. "\n"; 
  $edit_get.=' <!-- Breadcrumb -->'. "\n";    
  $edit_get.=' <nav class="hk-breadcrumb" aria-label="breadcrumb"> '. "\n"; 
  $edit_get.=' <ol class="breadcrumb breadcrumb-light bg-transparent">'. "\n"; 
  $edit_get.=' <li class="breadcrumb-item">'. "\n"; 
  $edit_get.=' <a href="#">Components </a>'. "\n"; 
  $edit_get.=' </li>'. "\n"; 
  $edit_get.=' <li class="breadcrumb-item active" aria-current="page"> '.$tabla.' - Edit&nbsp;&nbsp;</li>'. "\n"; 
  $edit_get.=' </ol>'. "\n"; 
  $edit_get.=' </nav>'. "\n"; 


  $edit_get.='<section class="hk-sec-wrapper">'. "\n"; 
  $edit_get.=' <?=form_open("'.$tablasinespacio.'/do_edit");?>'. "\n";
  $edit_get.='  <div class="position-center">'. "\n";
	  
  foreach($campos as $campo){
    if($campo==$keyprimaria){
     $edit_get.='<input type="hidden" name="'.$campo.'" id="'.$campo.'" value="<?php echo $'.$tabla.'["'.$campo.'"];?>"  class="form-control"  />'. "\n";      
    }else{    
  $edit_get.='<div class="form-group">'. "\n";
  $edit_get.='<label class="col-sm-3 control-label">'.$campo.'</label>'. "\n";
  $edit_get.='<div class="col-sm-8">'. "\n";
  $edit_get.='<input type="text" name="'.$campo.'" id="'.$campo.'" value="<?php echo $'.$tabla.'["'.$campo.'"];?>"  class="form-control"  />'. "\n";
  $edit_get.='</div>'. "\n";
  $edit_get.='</div>'. "\n";
    }
  
  
  }  

  $edit_get.='<div class="form-group"><div class="col-lg-offset-2 col-lg-10">'. "\n"; 
  $edit_get.='<input type="submit" value="GUARDAR" class="btn btn-primary">	'. "\n";
  $edit_get.='<a class="btn btn-default" href="<?php echo site_url("'.$tablasinespacio.'");?>">VOLVER</a> </div></div>'. "\n";
    
    
  $edit_get.='</form>'. "\n";
  $edit_get.=' </div>'. "\n";	
  $edit_get.='</section>'. "\n";	
  $edit_get.='<?= $this->endSection() ?>'. "\n";	 
  return $edit_get;            
 
  return $edit_get;    

}




?>
