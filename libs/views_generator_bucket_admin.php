<?php
/*++++++++++++++++++++++++++++++++++++++++++++++++
aporte por angelo romero ++++++++++++++++++++++++
visita 
https://bitbucket.org/angeloromero/botgen+++++++++++
para tener mans info sobre el proyecto++++++++++++++
gracias por tu visita y aporte  :D  +++++++++++++++
*/
function index_gen($tabla,$keyprimaria,$campos){
  $asfore='';
  $asfore=substr($tabla, 0, -1); 
  $outindex='';

    
     $outindex.=' <div class="col-md-12">'. "\n"; 
     $outindex.=' <section class="panel">'. "\n"; 
     $outindex.='  <header class="panel-heading">'. "\n"; 
    
    
    $outindex.=''.$tabla.' - Index&nbsp;&nbsp;'. "\n";
    $outindex.='<?php echo anchor("'.$tabla;
    $outindex.='/add';
    $outindex.='", "AGREGAR NUEVO REGISTRO",array("class" => "btn btn-primary")); ?>'. "\n";
    $outindex.='<span class="tools pull-right">'. "\n";
    
     $outindex.='<a href="javascript:;" class="fa fa-chevron-down"></a>'. "\n"; 
    
     $outindex.='</span>'. "\n"; 
     $outindex.='</header>'. "\n"; 


     $outindex.='<div class="panel-body">'. "\n"; 
     $outindex.='<div class="adv-table editable-table" style="overflow-x:scroll">'. "\n"; 
    

    
  $outindex.='<table class="table table-striped table-hover table-bordered" id="editable-sample">'. "\n";
  $outindex.='<thead>'. "\n";
  $outindex.='<tr align="center">'. "\n";
  foreach($campos as $campo){
  $outindex.='<th>'.$campo.'</th>'. "\n";
  
  }
  $outindex.='<th>Editar</th>'. "\n";
  $outindex.='<th>Borrar</th>'. "\n";    
  $outindex.='</tr>'. "\n";     
  $outindex.='</thead>'. "\n";     
  $outindex.='<tbody>'. "\n"; 

  $outindex.='<?php foreach ($'.$tabla.' as $'.$asfore.') { ?>'. "\n";     
  $outindex.='<tr align="center">'. "\n"; 
  foreach($campos as $campo){
  $outindex.='<td><?php echo $'.$asfore.'->'.$campo.';?></td>'. "\n";
  }
  $outindex.='<td><?php echo anchor("'.$tabla;
  $outindex.='/edit/".$' .$asfore.'->'.$keyprimaria.', "Editar",array("class" => "btn btn-success")); ?></td>'."\n";
  $outindex.='<td><?php echo anchor("'.$tabla;
  $outindex.='/delete/".$'.$asfore.'->'.$keyprimaria.', "Editar",array("class" => "btn btn-danger")); ?></td>'."\n";
  $outindex.='</tr>'. "\n";     
  $outindex.='<?php } ?>'. "\n";     
  $outindex.='</tbody>'. "\n";         
  $outindex.='</table>'. "\n";         
  $outindex.='</div></div></section></div>'. "\n"; 


  
  $outindex.='<script>'. "\n";  
  $outindex.=' jQuery(document).ready(function() {'. "\n";  
  $outindex.=  'EditableTable.init();'. "\n";  
  $outindex.='   });'. "\n";  
  $outindex.='</script>'. "\n";  



      
 
 
  return $outindex;    

}


function add_gen($tabla,$keyprimaria,$campos){
  $asfore='';
  $asfore=substr($tabla, 0, -1); 
  $edit_get='';
  $edit_get.='<div class="col-lg-12">'. "\n"; 
  $edit_get.=' <section class="panel">'. "\n"; 
  $edit_get.=' <header class="panel-heading">'. "\n"; 
  $edit_get.=$tabla.' Agregar'. "\n";  
  $edit_get.=' </header>'. "\n"; 
  $edit_get.=' <div class="panel-body">'. "\n"; 
  $edit_get.=' <div class="position-center">'. "\n"; 

  $edit_get.=' <form class="form-horizontal form-groups-bordered" role="form" accept-charset="UTF-8" action="<?php echo site_url("'.$tabla.'/do_edit");?>" method="POST">'. "\n";
	  
  foreach($campos as $campo){
    if($campo==$keyprimaria){
     $edit_get.='<input type="hidden" name="'.$campo.'" id="'.$campo.'" value=""  class="form-control"  />'. "\n";      
    }else{    
  $edit_get.='<div class="form-group">'. "\n";
  $edit_get.='<label class="col-sm-3 control-label">'.$campo.'</label>'. "\n";
  $edit_get.='<div class="col-sm-8">'. "\n";
  $edit_get.='<input type="text" name="'.$campo.'" id="'.$campo.'" value=""  class="form-control"  />'. "\n";
  $edit_get.='</div>'. "\n";
  $edit_get.='</div>'. "\n";
    }
  
  
  }  

  $edit_get.='<div class="form-group"><div class="col-lg-offset-2 col-lg-10">'. "\n"; 
  $edit_get.='<input type="submit" value="GUARDAR" class="btn btn-primary">	'. "\n";
  $edit_get.='<a class="btn btn-default" href="<?php echo site_url("'.$tabla.'");?>">VOLVER</a> </div></div>'. "\n";
    
    
  $edit_get.='</form>'. "\n";
		
  $edit_get.='</div>'. "\n";         
  $edit_get.='</div>'. "\n";         
  $edit_get.='</div>'. "\n";         
 
  return $edit_get;    

}


/*
function add_gen($tabla,$keyprimaria,$campos){
	
  $asfore='';
  $asfore=substr($tabla, 0, -1); 
  $edit_get='';   
  $edit_get.='<div class="col-lg-12">'. "\n"; 
  $edit_get.=' <section class="panel">'. "\n"; 
  $edit_get.=' <header class="panel-heading">'. "\n"; 
  $edit_get.=$tabla.' Editar'. "\n";  
  $edit_get.=' </header>'. "\n"; 
  $edit_get.=' <div class="panel-body">'. "\n"; 
  $edit_get.=' <div class="position-center">'. "\n"; 
    
    
  $edit_get.=' <form class="form-horizontal form-groups-bordered" role="form" accept-charset="UTF-8" action="<?php echo site_url("'.$tabla.'/do_add");?>" method="POST">'. "\n";
	  
  foreach($campos as $campo){
        if($campo!=$keyprimaria){
  
            
                              
            
  $edit_get.='<div class="form-group">'. "\n";
  $edit_get.='<label class="col-lg-2 col-sm-2 control-label">'.$campo.'</label>'. "\n";
  $edit_get.='<div class="col-lg-10">'. "\n";
  $edit_get.='<input type="text" name="'.$campo.'" id="'.$campo.'" value="" class="form-control"  />'. "\n";
  $edit_get.='</div>'. "\n";
  $edit_get.='</div>'. "\n";
        }
  }  
    
    
  $edit_get.='<div class="form-group"><div class="col-lg-offset-2 col-lg-10">'. "\n";
 
  $edit_get.='<input type="submit" value="GUARDAR" class="btn btn-primary">	'. "\n";
  $edit_get.='<a class="btn btn-default" href="<?php echo site_url("'.$tabla.'");?>">VOLVER</a> </div></div>'. "\n";
  
  $edit_get.='</form>'. "\n";
		
  $edit_get.='</div>'. "\n";         
  $edit_get.='</div>'. "\n";         
  $edit_get.='</div>'. "\n";         
  return $edit_get;    

}*/



function edit_gen($tabla,$keyprimaria,$campos){
  $asfore='';
  $asfore=substr($tabla, 0, -1); 
  $edit_get='';
  $edit_get.='<div class="col-lg-12">'. "\n"; 
  $edit_get.=' <section class="panel">'. "\n"; 
  $edit_get.=' <header class="panel-heading">'. "\n"; 
  $edit_get.=$tabla.' Editar'. "\n";  
  $edit_get.=' </header>'. "\n"; 
  $edit_get.=' <div class="panel-body">'. "\n"; 
  $edit_get.=' <div class="position-center">'. "\n"; 

  $edit_get.=' <form class="form-horizontal form-groups-bordered" role="form" accept-charset="UTF-8" action="<?php echo site_url("'.$tabla.'/do_edit");?>" method="POST">'. "\n";
	  
  foreach($campos as $campo){
    if($campo==$keyprimaria){
     $edit_get.='<input type="hidden" name="'.$campo.'" id="'.$campo.'" value="<?php echo $'.$tabla.'["'.$campo.'"];?>"  class="form-control"  />'. "\n";      
    }else{    
  $edit_get.='<div class="form-group">'. "\n";
  $edit_get.='<label class="col-sm-3 control-label">'.$campo.'</label>'. "\n";
  $edit_get.='<div class="col-sm-8">'. "\n";
  $edit_get.='<input type="text" name="'.$campo.'" id="'.$campo.'" value="<?php echo $'.$tabla.'["'.$campo.'"];?>"  class="form-control"  />'. "\n";
  $edit_get.='</div>'. "\n";
  $edit_get.='</div>'. "\n";
    }
  
  
  }  

  $edit_get.='<div class="form-group"><div class="col-lg-offset-2 col-lg-10">'. "\n"; 
  $edit_get.='<input type="submit" value="GUARDAR" class="btn btn-primary">	'. "\n";
  $edit_get.='<a class="btn btn-default" href="<?php echo site_url("'.$tabla.'");?>">VOLVER</a> </div></div>'. "\n";
    
    
  $edit_get.='</form>'. "\n";
		
  $edit_get.='</div>'. "\n";         
  $edit_get.='</div>'. "\n";         
  $edit_get.='</div>'. "\n";         
 
  return $edit_get;    

}




?>
