<?php   
/*++++++++++++++++++++++++++++++++++++++++++++++++
aporte por angelo romero ++++++++++++++++++++++++
visita 
https://bitbucket.org/angeloromero/botgen+++++++++++
para tener mans info sobre el proyecto++++++++++++++
gracias por tu visita y aporte  :D  +++++++++++++++
*/
function generador($tabla,$primary_key){

    $output="";
    $output.="<?php //if ( ! defined('BASEPATH')) exit('No direct script access allowed');\n";
    $output.=" namespace App\Models;\n";
    $output.=" use CodeIgniter\Model;\n";
    $output.=" class ".ucfirst($tabla). "_model extends Model { \n";
    $output.='var $table ='."'".$tabla."'".";\n";
    $output.="/*function ".ucfirst($tabla)."_model(){ \n";
    $output.="parent::__construct(); } \n";

	
    
    
	$output.='function get_by_id($id){'. "\n";
    $output.='$this->db->where('."'".$primary_key."'";
    $output.=', $id);'."\n";
    $output.='$query =$this->db->get($this->table);' . "\n";
	$output.='return $query->row();' . "\n";
	$output.='}'. "\n";

	$output.='function count_all(){'. "\n";;
    $output.='return $this->db->count_all($this->table);'. "\n";
	$output.='}'. "\n";

    $output.='function insert($data){'. "\n";
    $output.='$this->db->insert($this->table, $data);' . "\n";
	$output.='}'. "\n";
	
    $output.='function update($data, $id){'. "\n";
    $output.='$this->db->where('."'".$primary_key."'";
    $output.=', $id);'."\n";
    $output.='$this->db->update($this->table, $data);' . "\n";
	$output.='}'. "\n";

    $output.='function delete($id){'. "\n";
    $output.='$this->db->where('."'".$primary_key."'";
    $output.=', $id);'."\n";
    $output.='$this->db->delete($this->table);' . "\n";
	$output.='}'. "\n";

    $output.='function get_all($limit = null, $offset = null){'. "\n";
    $output.='$this->db->order_by('."'".$primary_key."'";
    $output.=", 'desc');"."\n";
    $output.='	$query = $this->db->get($this->table, $limit, $offset);' . "\n";
	$output.='return $query->result();'. "\n";
	$output.='}'. "\n";

	$output.='function get_all_array($limit = null, $offset = null){'. "\n";
    $output.='$this->db->order_by('."'".$primary_key."'";
    $output.=", 'desc');"."\n";
    $output.='	$query = $this->db->get($this->table, $limit, $offset);' . "\n";
	$output.='return $query->result_array();'. "\n";
	$output.='}*/'. "\n";
    
    $output.=" } \n";
    
    
    
    return $output;
}
?>