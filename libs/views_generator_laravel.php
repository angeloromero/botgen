<?php
/*++++++++++++++++++++++++++++++++++++++++++++++++
aporte por angelo romero ++++++++++++++++++++++++
visita 
https://bitbucket.org/angeloromero/botgen +++++++++++
para tener mas info sobre el proyecto++++++++++++++
gracias por tu visita y aporte  :D  +++++++++++++++
*/
function index_gen($tabla,$keyprimaria,$campos){
  $asfore='';
  $asfore=substr($tabla, 0, -1); 
  $outindex='';
  $outindex.='<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">-->'. "\n";
    
  $outindex.='@extends("layouts.default") '. "\n";
  $outindex.='@section("content")'. "\n";
  $outindex.='<div class="widget-box">'. "\n";
  $outindex.='  <div class="widget-title">'. "\n";
  $outindex.='  <span class="icon"><i class="icon-th"></i></span>'. "\n";
  $outindex.='  <h5>'.$tabla.' - Index</h5>'. "\n";
  $outindex.='<p><a class="btn btn-small btn-green" href="{{ url("'.str_replace('_','',$tabla).'/add/")}}">Agregar nuevo</a></p>'. "\n";
  $outindex.='</div>'. "\n";
  $outindex.=' <div class="widget-content nopadding">'. "\n";    
  $outindex.='<table class="table table-bordered data-table dataTable" id="DataTables_Table_0" border="1">'. "\n";
  $outindex.='<thead>'. "\n";
  $outindex.='<tr align="center">'. "\n";
  foreach($campos as $campo){
  $outindex.='<td>'.$campo.'</td>'. "\n";
  }
  $outindex.='<td>Editar</td>'. "\n";
  $outindex.='<td>Borrar</td>'. "\n";
  $outindex.='</tr>'. "\n";     
  $outindex.='</thead>'. "\n";     
  $outindex.='<tbody>'. "\n"; 

  $outindex.='<?php foreach ($'.$tabla.' as $'.$asfore.') { ?>'. "\n";     
  $outindex.='<tr align="center">'. "\n"; 
  foreach($campos as $campo){
    $outindex.='<td>{{ $'.$asfore.'->'.$campo.'}}</td>'. "\n";
  }
  $outindex.='<td><a class="btn btn-small btn-orange" href="{{ url("'.str_replace('_','',$tabla).'/edit/")}}/{{$'.$asfore.'->'.$keyprimaria.'}}">Editar</a></td>';
  $outindex.='<td><a class="btn btn-small btn-red" href="{{ url("'.str_replace('_','',$tabla).'/edit/")}}/{{$'.$asfore.'->'.$keyprimaria.'}}">Borrar</a></td>';
  
  $outindex.='</tr>'. "\n";     
  $outindex.='<?php } ?>'. "\n";     
  $outindex.='</tbody>'. "\n";         
  $outindex.='</table>'. "\n";         
  $outindex.='</div>'. "\n";  
    $outindex.='@stop'. "\n";
  return $outindex;    

}

function edit_gen($tabla,$keyprimaria,$campos){
  $asfore='';
  $asfore=substr($tabla, 0, -1); 
  $edit_get='';
  $edit_get.='@extends("layouts.default")'. "\n";
  $edit_get.='@section("content")'. "\n";
  $edit_get.=' <div class="row">'. "\n";
  $edit_get.='<div class="col-md-12">'. "\n";
  $edit_get.=' <div class="panel panel-primary" data-collapsed="0"> '. "\n";
  $edit_get.=' <div class="panel-heading"> '. "\n";
  $edit_get.=' <div class="panel-title"> '. "\n";  
  $edit_get.=$tabla.' Editar'. "\n";
  $edit_get.=' </div>'. "\n"; 
  
  
  
  $edit_get.=' <div class="panel-options"> '. "\n";
  $edit_get.='<a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a> '. "\n";
  $edit_get.=' <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a> '. "\n";
  $edit_get.='<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a> '. "\n";
  $edit_get.=' <!--<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>-->'. "\n";  
  $edit_get.='</div>'. "\n";
  $edit_get.=' </div>'. "\n";
  
  
 
  
  $edit_get.='  <div class="panel-body">'. "\n";
  $edit_get.=' <form class="form-horizontal form-groups-bordered" role="form" accept-charset="UTF-8" action="{{url("'.str_replace('_','',$tabla).'/update")}}" method="POST">'. "\n";
	  
  foreach($campos as $campo){
    if($campo==$keyprimaria){
     $edit_get.='<input type="hidden" name="'.$campo.'" id="'.$campo.'" value="{{$'.$tabla.'->'.$campo.'}}"  class="form-control"  />'. "\n";      
    }else{    
  $edit_get.='<div class="form-group">'. "\n";
  $edit_get.='<label class="col-sm-3 control-label">'.$campo.'</label>'. "\n";
  $edit_get.='<div class="col-sm-8">'. "\n";
  $edit_get.='<input type="text" name="'.$campo.'" id="'.$campo.'" value="{{$'.$tabla.'->'.$campo.'}}"  class="form-control"  />'. "\n";
  $edit_get.='</div>'. "\n";
  $edit_get.='</div>'. "\n";
    }
  
  
  }  
  $edit_get.='	<label class="col-sm-3 control-label" for="do_action">Guardar</label>'. "\n";
  $edit_get.='<div class="controls">'. "\n";
  $edit_get.='</div>'. "\n";
  $edit_get.='<input type="submit" value="GUARDAR" class="btn btn-blue">	'. "\n";
  $edit_get.='<a class="btn btn-default" href="{{url("'.str_replace('_','',$tabla).'/")}}">VOLVER</a>	'. "\n";
  
  $edit_get.='</form>'. "\n";
		
  $edit_get.='</div>'. "\n";         
  $edit_get.='</div>'. "\n";         
  $edit_get.='</div>'. "\n";         
       
  $edit_get.='@stop'. "\n";         
  return $edit_get;    

}


function add_gen($tabla,$keyprimaria,$campos){
  $asfore='';
  $asfore=substr($tabla, 0, -1); 
  $edit_get='';
  $edit_get.='@extends("layouts.default")'. "\n";
  $edit_get.='@section("content")'. "\n";
  $edit_get.=' <div class="row">'. "\n";
  $edit_get.='<div class="col-md-12">'. "\n";
  $edit_get.=' <div class="panel panel-primary" data-collapsed="0"> '. "\n";
  $edit_get.=' <div class="panel-heading"> '. "\n";
  $edit_get.=' <div class="panel-title"> '. "\n";  
  $edit_get.=$tabla.' Editar'. "\n";
  $edit_get.=' </div>'. "\n"; 
    
  $edit_get.=' <div class="panel-options"> '. "\n";
  $edit_get.='<a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a> '. "\n";
  $edit_get.=' <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a> '. "\n";
  $edit_get.='<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a> '. "\n";
  $edit_get.=' <!--<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>-->'. "\n";  
  $edit_get.='</div>'. "\n";
  $edit_get.=' </div>'. "\n";
  
  
 
  
  $edit_get.='  <div class="panel-body">'. "\n";
  $edit_get.=' <form class="form-horizontal form-groups-bordered" role="form" accept-charset="UTF-8" action="{{url("'.str_replace('_','',$tabla).'/store")}}" method="POST">'. "\n";
	  
  foreach($campos as $campo){
        if($campo!=$keyprimaria){
  
  $edit_get.='<div class="form-group">'. "\n";
  $edit_get.='<label class="col-sm-3 control-label">'.$campo.'</label>'. "\n";
  $edit_get.='<div class="col-sm-8">'. "\n";
  $edit_get.='<input type="text" name="'.$campo.'" id="'.$campo.'" value="" class="form-control"  />'. "\n";
  $edit_get.='</div>'. "\n";
  $edit_get.='</div>'. "\n";
        }
  }  
  $edit_get.='	<label class="col-sm-3 control-label" for="do_action">Guardar</label>'. "\n";
  $edit_get.='<div class="controls">'. "\n";
  $edit_get.='</div>'. "\n";
  $edit_get.='<input type="submit" value="GUARDAR" class="btn btn-blue">	'. "\n";
  $edit_get.='<a class="btn btn-default" href="{{url("'.str_replace('_','',$tabla).'/")}}">VOLVER</a>	'. "\n";
  
  $edit_get.='</form>'. "\n";
		
  $edit_get.='</div>'. "\n";         
  $edit_get.='</div>'. "\n";         
  $edit_get.='</div>'. "\n";         
       
  $edit_get.='@stop'. "\n";         
  return $edit_get;    

}











?>
