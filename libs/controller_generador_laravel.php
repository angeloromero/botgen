<?php 
/*++++++++++++++++++++++++++++++++++++++++++++++++
aporte por angelo romero ++++++++++++++++++++++++
visita 
https://bitbucket.org/angeloromero/botgen+++++++++++
para tener mans info sobre el proyecto++++++++++++++
gracias por tu visita y aporte  :D  +++++++++++++++
*/
function generador_comtlara($tabla,$primary_key,$campos){

    $outcont="";
    //$output.='<?php session_start();'
    $outcont.='<?php  //Route::controller("'.strtolower(str_replace('_','',$tabla)).'","'.ucfirst((str_replace('_','',$tabla))).'Controller");<--agregar a routes.php;'. "\n";
    $outcont.=" class ".ucfirst(str_replace('_','',$tabla))."Controller extends BaseController { \n";
    $outcont.="function __construct(){". "\n";
    $outcont.="}". "\n";
    $outcont.="function getIndex(){". "\n";   
    $outcont.='$data = array();'. "\n";     
    $outcont.='$data["'.$tabla.'"] ='.ucfirst((str_replace('_','',$tabla))).'::all();'. "\n";        
    $outcont.='return View::make("'.$tabla.'.index';
    $outcont.='",$data);'. "\n";        
    $outcont.="}". "\n";
    
    $outcont.="function getAdd(){". "\n";
    $outcont.='return View::make("'.$tabla.".add";
    $outcont.='");'. "\n";        
    $outcont.="}". "\n";
    $outcont.="\n";
    
    
    $outcont.="function postStore(){". "\n";
    $outcont.='$data=array();'. "\n";
    $outcont.='$data = Input::all();'. "\n";
    foreach($campos as $fields){
   
    if($fields!=$primary_key){
    $outcont.='$data["'.$fields.'"] = $data["'.$fields.'"];'. "\n";      
        }
    
        
    }    
    $outcont.=ucfirst((str_replace('_','',$tabla))).'::create($data);'.  "\n";
    $outcont.='return Redirect::to("'.str_replace('_','',$tabla).'");'. "\n";  
    $outcont.="}". "\n". "\n";

	
    $outcont.='function getEdit($id){'. "\n";
	$outcont.='$data["'.$tabla.'"]='.ucfirst((str_replace('_','',$tabla))).'::find($id);'. "\n";
    $outcont.='return View::make("'.$tabla.'.edit",$data);'. "\n";
    $outcont.="}". "\n";

    
    
    $outcont.="function postUpdate(){". "\n";
    $outcont.='$data=array();'. "\n";
    $outcont.='$data = Input::all();'. "\n";
    $outcont.='$'.$tabla.'='.ucfirst((str_replace('_','',$tabla))).'::find($data["'.$primary_key.'"]);';

    foreach($campos as $fields){
        if($fields!=$primary_key){
    $outcont.='$'.$tabla.'->'.$fields.'=$data["'.$fields.'"];'."\n";        
        }
    
    }    
    $outcont.='$'.$tabla.'->save();'.  "\n";
    $outcont.='return Redirect::to("'.str_replace('_','',$tabla).'");'. "\n";  
    $outcont.="}". "\n". "\n";

    
    
    
    
    
    $outcont.="function delete(){". "\n";
    $outcont.='if ($this->uri->segment(3) === FALSE)'. "\n";
    $outcont.='redirect("'.$tabla.'");'. "\n";
    $outcont.='$id = (int)$this->uri->segment(3);'. "\n";
    $outcont.='$this->'.$tabla.'_model->delete($id);'. "\n";
    $outcont.='redirect('.$tabla.');'. "\n";  
    $outcont.="}". "\n";
    
    



    
    $outcont.="}". "\n";
    
    return $outcont;
}
?>