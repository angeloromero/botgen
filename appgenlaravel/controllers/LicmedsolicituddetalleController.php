<?php  //Route::controller("licmedsolicituddetalle","LicmedsolicituddetalleController");<--agregar a routes.php;
 class LicmedsolicituddetalleController extends BaseController { 
function __construct(){
}
function getIndex(){
$data = array();
$data["licmed_solicitud_detalle"] =Licmedsolicituddetalle::all();
return View::make("licmed_solicitud_detalle.index",$data);
}
function getAdd(){
return View::make("licmed_solicitud_detalle.add");
}

function postStore(){
$data=array();
$data = Input::all();
$data["FECHA_INICIO"] = $data["FECHA_INICIO"];
$data["FECHA_FIN"] = $data["FECHA_FIN"];
$data["FKID_SOLICITUD"] = $data["FKID_SOLICITUD"];
$data["FKID_TIPODOCUMENTO"] = $data["FKID_TIPODOCUMENTO"];
$data["ID_REPO"] = $data["ID_REPO"];
$data["RUTA_REPO"] = $data["RUTA_REPO"];
$data["SESSION"] = $data["SESSION"];
$data["UPDATED_AT"] = $data["UPDATED_AT"];
$data["CREATED_AT"] = $data["CREATED_AT"];
$data["DELETED_AT"] = $data["DELETED_AT"];
Licmedsolicituddetalle::create($data);
return Redirect::to("licmedsolicituddetalle");
}

function getEdit($id){
$data["licmed_solicitud_detalle"]=Licmedsolicituddetalle::find($id);
return View::make("licmed_solicitud_detalle.edit",$data);
}
function postUpdate(){
$data=array();
$data = Input::all();
$licmed_solicitud_detalle=Licmedsolicituddetalle::find($data["ID"]);$licmed_solicitud_detalle->FECHA_INICIO=$data["FECHA_INICIO"];
$licmed_solicitud_detalle->FECHA_FIN=$data["FECHA_FIN"];
$licmed_solicitud_detalle->FKID_SOLICITUD=$data["FKID_SOLICITUD"];
$licmed_solicitud_detalle->FKID_TIPODOCUMENTO=$data["FKID_TIPODOCUMENTO"];
$licmed_solicitud_detalle->ID_REPO=$data["ID_REPO"];
$licmed_solicitud_detalle->RUTA_REPO=$data["RUTA_REPO"];
$licmed_solicitud_detalle->SESSION=$data["SESSION"];
$licmed_solicitud_detalle->UPDATED_AT=$data["UPDATED_AT"];
$licmed_solicitud_detalle->CREATED_AT=$data["CREATED_AT"];
$licmed_solicitud_detalle->DELETED_AT=$data["DELETED_AT"];
$licmed_solicitud_detalle->save();
return Redirect::to("licmedsolicituddetalle");
}

function delete(){
if ($this->uri->segment(3) === FALSE)
redirect("licmed_solicitud_detalle");
$id = (int)$this->uri->segment(3);
$this->licmed_solicitud_detalle_model->delete($id);
redirect(licmed_solicitud_detalle);
}
}
