<?php  //Route::controller("bolempleabilidad","BolempleabilidadController");<--agregar a routes.php;
 class BolempleabilidadController extends BaseController { 
function __construct(){
}
function getIndex(){
$data = array();
$data["bol_empleabilidad"] =Bolempleabilidad::all();
return View::make("bol_empleabilidad.index",$data);
}
function getAdd(){
return View::make("bol_empleabilidad.add");
}

function postStore(){
$data=array();
$data = Input::all();
$data["FKID_PERSONA"] = $data["FKID_PERSONA"];
$data["MAIL_ALTERNATIVO"] = $data["MAIL_ALTERNATIVO"];
$data["FONO1"] = $data["FONO1"];
$data["FONO2"] = $data["FONO2"];
$data["ACEPTA_TERMS"] = $data["ACEPTA_TERMS"];
$data["CREATED_AT"] = $data["CREATED_AT"];
$data["UPDATED_AT"] = $data["UPDATED_AT"];
$data["DELETED_AT"] = $data["DELETED_AT"];
Bolempleabilidad::create($data);
return Redirect::to("bolempleabilidad");
}

function getEdit($id){
$data["bol_empleabilidad"]=Bolempleabilidad::find($id);
return View::make("bol_empleabilidad.edit",$data);
}
function postUpdate(){
$data=array();
$data = Input::all();
$bol_empleabilidad=Bolempleabilidad::find($data["ID"]);$bol_empleabilidad->FKID_PERSONA=$data["FKID_PERSONA"];
$bol_empleabilidad->MAIL_ALTERNATIVO=$data["MAIL_ALTERNATIVO"];
$bol_empleabilidad->FONO1=$data["FONO1"];
$bol_empleabilidad->FONO2=$data["FONO2"];
$bol_empleabilidad->ACEPTA_TERMS=$data["ACEPTA_TERMS"];
$bol_empleabilidad->CREATED_AT=$data["CREATED_AT"];
$bol_empleabilidad->UPDATED_AT=$data["UPDATED_AT"];
$bol_empleabilidad->DELETED_AT=$data["DELETED_AT"];
$bol_empleabilidad->save();
return Redirect::to("bolempleabilidad");
}

function delete(){
if ($this->uri->segment(3) === FALSE)
redirect("bol_empleabilidad");
$id = (int)$this->uri->segment(3);
$this->bol_empleabilidad_model->delete($id);
redirect(bol_empleabilidad);
}
}
