<?php  //Route::controller("licmedsolicituddetallelogs","LicmedsolicituddetallelogsController");<--agregar a routes.php;
 class LicmedsolicituddetallelogsController extends BaseController { 
function __construct(){
}
function getIndex(){
$data = array();
$data["licmed_solicitud_detalle_logs"] =Licmedsolicituddetallelogs::all();
return View::make("licmed_solicitud_detalle_logs.index",$data);
}
function getAdd(){
return View::make("licmed_solicitud_detalle_logs.add");
}

function postStore(){
$data=array();
$data = Input::all();
$data["FKID_ESTADO"] = $data["FKID_ESTADO"];
$data["FKIDLICMED_SOLICITUD_DETALLE"] = $data["FKIDLICMED_SOLICITUD_DETALLE"];
$data["SESSION"] = $data["SESSION"];
$data["CREATED_AT"] = $data["CREATED_AT"];
$data["UPDATED_AT"] = $data["UPDATED_AT"];
$data["DELETED_AT"] = $data["DELETED_AT"];
Licmedsolicituddetallelogs::create($data);
return Redirect::to("licmedsolicituddetallelogs");
}

function getEdit($id){
$data["licmed_solicitud_detalle_logs"]=Licmedsolicituddetallelogs::find($id);
return View::make("licmed_solicitud_detalle_logs.edit",$data);
}
function postUpdate(){
$data=array();
$data = Input::all();
$licmed_solicitud_detalle_logs=Licmedsolicituddetallelogs::find($data["ID"]);$licmed_solicitud_detalle_logs->FKID_ESTADO=$data["FKID_ESTADO"];
$licmed_solicitud_detalle_logs->FKIDLICMED_SOLICITUD_DETALLE=$data["FKIDLICMED_SOLICITUD_DETALLE"];
$licmed_solicitud_detalle_logs->SESSION=$data["SESSION"];
$licmed_solicitud_detalle_logs->CREATED_AT=$data["CREATED_AT"];
$licmed_solicitud_detalle_logs->UPDATED_AT=$data["UPDATED_AT"];
$licmed_solicitud_detalle_logs->DELETED_AT=$data["DELETED_AT"];
$licmed_solicitud_detalle_logs->save();
return Redirect::to("licmedsolicituddetallelogs");
}

function delete(){
if ($this->uri->segment(3) === FALSE)
redirect("licmed_solicitud_detalle_logs");
$id = (int)$this->uri->segment(3);
$this->licmed_solicitud_detalle_logs_model->delete($id);
redirect(licmed_solicitud_detalle_logs);
}
}
