<?php
 class Licmedsolicituddetalle extends Eloquent  { 
protected $table ='licmed_solicitud_detalle';
protected $primaryKey ='ID';
public $timestamps = false;
protected $fillable=[
"FECHA_INICIO",
"FECHA_FIN",
"FKID_SOLICITUD",
"FKID_TIPODOCUMENTO",
"ID_REPO",
"RUTA_REPO",
"SESSION",
"UPDATED_AT",
"CREATED_AT",
"DELETED_AT",
];

 } 
