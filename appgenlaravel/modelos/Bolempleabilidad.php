<?php
 class Bolempleabilidad extends Eloquent  { 
protected $table ='bol_empleabilidad';
protected $primaryKey ='ID';
public $timestamps = false;
protected $fillable=[
"FKID_PERSONA",
"MAIL_ALTERNATIVO",
"FONO1",
"FONO2",
"ACEPTA_TERMS",
"CREATED_AT",
"UPDATED_AT",
"DELETED_AT",
];

 } 
