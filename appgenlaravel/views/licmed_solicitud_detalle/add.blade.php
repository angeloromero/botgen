@extends("layouts.default")
@section("content")
 <div class="row">
<div class="col-md-12">
 <div class="panel panel-primary" data-collapsed="0"> 
 <div class="panel-heading"> 
 <div class="panel-title"> 
licmed_solicitud_detalle Editar
 </div>
 <div class="panel-options"> 
<a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a> 
 <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a> 
<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a> 
 <!--<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>-->
</div>
 </div>
  <div class="panel-body">
 <form class="form-horizontal form-groups-bordered" role="form" accept-charset="UTF-8" action="{{url("licmedsolicituddetalle/store")}}" method="POST">
<div class="form-group">
<label class="col-sm-3 control-label">FECHA_INICIO</label>
<div class="col-sm-8">
<input type="text" name="FECHA_INICIO" id="FECHA_INICIO" value="" class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">FECHA_FIN</label>
<div class="col-sm-8">
<input type="text" name="FECHA_FIN" id="FECHA_FIN" value="" class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">FKID_SOLICITUD</label>
<div class="col-sm-8">
<input type="text" name="FKID_SOLICITUD" id="FKID_SOLICITUD" value="" class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">FKID_TIPODOCUMENTO</label>
<div class="col-sm-8">
<input type="text" name="FKID_TIPODOCUMENTO" id="FKID_TIPODOCUMENTO" value="" class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">ID_REPO</label>
<div class="col-sm-8">
<input type="text" name="ID_REPO" id="ID_REPO" value="" class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">RUTA_REPO</label>
<div class="col-sm-8">
<input type="text" name="RUTA_REPO" id="RUTA_REPO" value="" class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">SESSION</label>
<div class="col-sm-8">
<input type="text" name="SESSION" id="SESSION" value="" class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">UPDATED_AT</label>
<div class="col-sm-8">
<input type="text" name="UPDATED_AT" id="UPDATED_AT" value="" class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">CREATED_AT</label>
<div class="col-sm-8">
<input type="text" name="CREATED_AT" id="CREATED_AT" value="" class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">DELETED_AT</label>
<div class="col-sm-8">
<input type="text" name="DELETED_AT" id="DELETED_AT" value="" class="form-control"  />
</div>
</div>
	<label class="col-sm-3 control-label" for="do_action">Guardar</label>
<div class="controls">
</div>
<input type="submit" value="GUARDAR" class="btn btn-blue">	
<a class="btn btn-default" href="{{url("licmedsolicituddetalle/")}}">VOLVER</a>	
</form>
</div>
</div>
</div>
@stop
