<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">-->
@extends("layouts.default") 
@section("content")
<div class="widget-box">
  <div class="widget-title">
  <span class="icon"><i class="icon-th"></i></span>
  <h5>licmed_solicitud_detalle - Index</h5>
<p><a class="btn btn-small btn-green" href="{{ url("licmedsolicituddetalle/add/")}}">Agregar nuevo</a></p>
</div>
 <div class="widget-content nopadding">
<table class="table table-bordered data-table dataTable" id="DataTables_Table_0" border="1">
<thead>
<tr align="center">
<td>ID</td>
<td>FECHA_INICIO</td>
<td>FECHA_FIN</td>
<td>FKID_SOLICITUD</td>
<td>FKID_TIPODOCUMENTO</td>
<td>ID_REPO</td>
<td>RUTA_REPO</td>
<td>SESSION</td>
<td>UPDATED_AT</td>
<td>CREATED_AT</td>
<td>DELETED_AT</td>
<td>Editar</td>
<td>Borrar</td>
</tr>
</thead>
<tbody>
<?php foreach ($licmed_solicitud_detalle as $licmed_solicitud_detall) { ?>
<tr align="center">
<td>{{ $licmed_solicitud_detall->ID}}</td>
<td>{{ $licmed_solicitud_detall->FECHA_INICIO}}</td>
<td>{{ $licmed_solicitud_detall->FECHA_FIN}}</td>
<td>{{ $licmed_solicitud_detall->FKID_SOLICITUD}}</td>
<td>{{ $licmed_solicitud_detall->FKID_TIPODOCUMENTO}}</td>
<td>{{ $licmed_solicitud_detall->ID_REPO}}</td>
<td>{{ $licmed_solicitud_detall->RUTA_REPO}}</td>
<td>{{ $licmed_solicitud_detall->SESSION}}</td>
<td>{{ $licmed_solicitud_detall->UPDATED_AT}}</td>
<td>{{ $licmed_solicitud_detall->CREATED_AT}}</td>
<td>{{ $licmed_solicitud_detall->DELETED_AT}}</td>
<td><a class="btn btn-small btn-orange" href="{{ url("licmedsolicituddetalle/edit/")}}/{{$licmed_solicitud_detall->ID}}">Editar</a></td><td><a class="btn btn-small btn-red" href="{{ url("licmedsolicituddetalle/edit/")}}/{{$licmed_solicitud_detall->ID}}">Borrar</a></td></tr>
<?php } ?>
</tbody>
</table>
</div>
@stop
