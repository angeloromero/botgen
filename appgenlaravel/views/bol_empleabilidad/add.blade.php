@extends("layouts.default")
@section("content")
 <div class="row">
<div class="col-md-12">
 <div class="panel panel-primary" data-collapsed="0"> 
 <div class="panel-heading"> 
 <div class="panel-title"> 
bol_empleabilidad Editar
 </div>
 <div class="panel-options"> 
<a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a> 
 <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a> 
<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a> 
 <!--<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>-->
</div>
 </div>
  <div class="panel-body">
 <form class="form-horizontal form-groups-bordered" role="form" accept-charset="UTF-8" action="{{url("bolempleabilidad/store")}}" method="POST">
<div class="form-group">
<label class="col-sm-3 control-label">FKID_PERSONA</label>
<div class="col-sm-8">
<input type="text" name="FKID_PERSONA" id="FKID_PERSONA" value="" class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">MAIL_ALTERNATIVO</label>
<div class="col-sm-8">
<input type="text" name="MAIL_ALTERNATIVO" id="MAIL_ALTERNATIVO" value="" class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">FONO1</label>
<div class="col-sm-8">
<input type="text" name="FONO1" id="FONO1" value="" class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">FONO2</label>
<div class="col-sm-8">
<input type="text" name="FONO2" id="FONO2" value="" class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">ACEPTA_TERMS</label>
<div class="col-sm-8">
<input type="text" name="ACEPTA_TERMS" id="ACEPTA_TERMS" value="" class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">CREATED_AT</label>
<div class="col-sm-8">
<input type="text" name="CREATED_AT" id="CREATED_AT" value="" class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">UPDATED_AT</label>
<div class="col-sm-8">
<input type="text" name="UPDATED_AT" id="UPDATED_AT" value="" class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">DELETED_AT</label>
<div class="col-sm-8">
<input type="text" name="DELETED_AT" id="DELETED_AT" value="" class="form-control"  />
</div>
</div>
	<label class="col-sm-3 control-label" for="do_action">Guardar</label>
<div class="controls">
</div>
<input type="submit" value="GUARDAR" class="btn btn-blue">	
<a class="btn btn-default" href="{{url("bolempleabilidad/")}}">VOLVER</a>	
</form>
</div>
</div>
</div>
@stop
