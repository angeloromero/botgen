<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">-->
@extends("layouts.default") 
@section("content")
<div class="widget-box">
  <div class="widget-title">
  <span class="icon"><i class="icon-th"></i></span>
  <h5>bol_empleabilidad - Index</h5>
<p><a class="btn btn-small btn-green" href="{{ url("bolempleabilidad/add/")}}">Agregar nuevo</a></p>
</div>
 <div class="widget-content nopadding">
<table class="table table-bordered data-table dataTable" id="DataTables_Table_0" border="1">
<thead>
<tr align="center">
<td>ID</td>
<td>FKID_PERSONA</td>
<td>MAIL_ALTERNATIVO</td>
<td>FONO1</td>
<td>FONO2</td>
<td>ACEPTA_TERMS</td>
<td>CREATED_AT</td>
<td>UPDATED_AT</td>
<td>DELETED_AT</td>
<td>Editar</td>
<td>Borrar</td>
</tr>
</thead>
<tbody>
<?php foreach ($bol_empleabilidad as $bol_empleabilida) { ?>
<tr align="center">
<td>{{ $bol_empleabilida->ID}}</td>
<td>{{ $bol_empleabilida->FKID_PERSONA}}</td>
<td>{{ $bol_empleabilida->MAIL_ALTERNATIVO}}</td>
<td>{{ $bol_empleabilida->FONO1}}</td>
<td>{{ $bol_empleabilida->FONO2}}</td>
<td>{{ $bol_empleabilida->ACEPTA_TERMS}}</td>
<td>{{ $bol_empleabilida->CREATED_AT}}</td>
<td>{{ $bol_empleabilida->UPDATED_AT}}</td>
<td>{{ $bol_empleabilida->DELETED_AT}}</td>
<td><a class="btn btn-small btn-orange" href="{{ url("bolempleabilidad/edit/")}}/{{$bol_empleabilida->ID}}">Editar</a></td><td><a class="btn btn-small btn-red" href="{{ url("bolempleabilidad/edit/")}}/{{$bol_empleabilida->ID}}">Borrar</a></td></tr>
<?php } ?>
</tbody>
</table>
</div>
@stop
