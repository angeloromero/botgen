@extends("layouts.default")
@section("content")
 <div class="row">
<div class="col-md-12">
 <div class="panel panel-primary" data-collapsed="0"> 
 <div class="panel-heading"> 
 <div class="panel-title"> 
licmed_solicitud_detalle_logs Editar
 </div>
 <div class="panel-options"> 
<a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a> 
 <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a> 
<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a> 
 <!--<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>-->
</div>
 </div>
  <div class="panel-body">
 <form class="form-horizontal form-groups-bordered" role="form" accept-charset="UTF-8" action="{{url("licmedsolicituddetallelogs/update")}}" method="POST">
<input type="hidden" name="ID" id="ID" value="{{$licmed_solicitud_detalle_logs->ID}}"  class="form-control"  />
<div class="form-group">
<label class="col-sm-3 control-label">FKID_ESTADO</label>
<div class="col-sm-8">
<input type="text" name="FKID_ESTADO" id="FKID_ESTADO" value="{{$licmed_solicitud_detalle_logs->FKID_ESTADO}}"  class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">FKIDLICMED_SOLICITUD_DETALLE</label>
<div class="col-sm-8">
<input type="text" name="FKIDLICMED_SOLICITUD_DETALLE" id="FKIDLICMED_SOLICITUD_DETALLE" value="{{$licmed_solicitud_detalle_logs->FKIDLICMED_SOLICITUD_DETALLE}}"  class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">SESSION</label>
<div class="col-sm-8">
<input type="text" name="SESSION" id="SESSION" value="{{$licmed_solicitud_detalle_logs->SESSION}}"  class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">CREATED_AT</label>
<div class="col-sm-8">
<input type="text" name="CREATED_AT" id="CREATED_AT" value="{{$licmed_solicitud_detalle_logs->CREATED_AT}}"  class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">UPDATED_AT</label>
<div class="col-sm-8">
<input type="text" name="UPDATED_AT" id="UPDATED_AT" value="{{$licmed_solicitud_detalle_logs->UPDATED_AT}}"  class="form-control"  />
</div>
</div>
<div class="form-group">
<label class="col-sm-3 control-label">DELETED_AT</label>
<div class="col-sm-8">
<input type="text" name="DELETED_AT" id="DELETED_AT" value="{{$licmed_solicitud_detalle_logs->DELETED_AT}}"  class="form-control"  />
</div>
</div>
	<label class="col-sm-3 control-label" for="do_action">Guardar</label>
<div class="controls">
</div>
<input type="submit" value="GUARDAR" class="btn btn-blue">	
<a class="btn btn-default" href="{{url("licmedsolicituddetallelogs/")}}">VOLVER</a>	
</form>
</div>
</div>
</div>
@stop
