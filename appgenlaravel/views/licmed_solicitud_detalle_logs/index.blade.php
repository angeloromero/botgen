<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">-->
@extends("layouts.default") 
@section("content")
<div class="widget-box">
  <div class="widget-title">
  <span class="icon"><i class="icon-th"></i></span>
  <h5>licmed_solicitud_detalle_logs - Index</h5>
<p><a class="btn btn-small btn-green" href="{{ url("licmedsolicituddetallelogs/add/")}}">Agregar nuevo</a></p>
</div>
 <div class="widget-content nopadding">
<table class="table table-bordered data-table dataTable" id="DataTables_Table_0" border="1">
<thead>
<tr align="center">
<td>ID</td>
<td>FKID_ESTADO</td>
<td>FKIDLICMED_SOLICITUD_DETALLE</td>
<td>SESSION</td>
<td>CREATED_AT</td>
<td>UPDATED_AT</td>
<td>DELETED_AT</td>
<td>Editar</td>
<td>Borrar</td>
</tr>
</thead>
<tbody>
<?php foreach ($licmed_solicitud_detalle_logs as $licmed_solicitud_detalle_log) { ?>
<tr align="center">
<td>{{ $licmed_solicitud_detalle_log->ID}}</td>
<td>{{ $licmed_solicitud_detalle_log->FKID_ESTADO}}</td>
<td>{{ $licmed_solicitud_detalle_log->FKIDLICMED_SOLICITUD_DETALLE}}</td>
<td>{{ $licmed_solicitud_detalle_log->SESSION}}</td>
<td>{{ $licmed_solicitud_detalle_log->CREATED_AT}}</td>
<td>{{ $licmed_solicitud_detalle_log->UPDATED_AT}}</td>
<td>{{ $licmed_solicitud_detalle_log->DELETED_AT}}</td>
<td><a class="btn btn-small btn-orange" href="{{ url("licmedsolicituddetallelogs/edit/")}}/{{$licmed_solicitud_detalle_log->ID}}">Editar</a></td><td><a class="btn btn-small btn-red" href="{{ url("licmedsolicituddetallelogs/edit/")}}/{{$licmed_solicitud_detalle_log->ID}}">Borrar</a></td></tr>
<?php } ?>
</tbody>
</table>
</div>
@stop
